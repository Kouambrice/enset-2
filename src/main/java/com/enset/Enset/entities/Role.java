/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.enset.Enset.entities;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
@Entity
/**
 *
 * @author jahaelle
 */
public class Role  implements Serializable {
    @Id
    private String role;
    private String description;

    public String getRole() {
        return role;
    }

    public String getDescription() {
        return description;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Role() {
    }

    public Role(String role, String description) {
        this.role = role;
        this.description = description;
    }
    
    
}
